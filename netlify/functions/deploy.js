const NetlifyAPI = require('netlify');

const {NETLIFY_API_TOKEN} = process.env;
const client = new NetlifyAPI(NETLIFY_API_TOKEN);

const failure = {
  statusCode: 503,
  body: JSON.stringify({}),
};

exports.handler = async function(event, context) {
  try {
    const body = JSON.parse(event.body);
    const {deployData} = body;
    if (!deployData || deployData === '') {
      console.log('deploy data missing or empty');
      return failure;
    }

    //AFEAB6ED3-A748-445C-8A56-C2AE3E422202^60C2C4DC2BBA950007CEE9BA
    const [site_id, deploy_id] = deployData.toUpperCase().split('^');
    if (!site_id || !deploy_id) {
      console.log('missing site id or deploy id');
      return failure;
    }

    //Check that it's valid
    const deploy = await client.getSiteDeploy({site_id, deploy_id});
    const {id, state} = deploy;
    if (id.toUpperCase() !== deploy_id.toUpperCase() || state !== 'ready') {
      console.log('id mismatch or not ready');
      return failure;
    }

    const result = await client.restoreSiteDeploy({site_id, deploy_id});
    const {url} = result;

    return {
      statusCode: 200,
      body: JSON.stringify({url}),
    };
  } catch (e) {
    console.log(e);
  }
  return failure;
};
