import React, {useRef, useEffect, useState} from 'react';

import {If, Then, Else, When} from 'react-if';

import Accordion from 'react-bootstrap/Accordion';
import Alert from 'react-bootstrap/Alert';
import Card from 'react-bootstrap/Card';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import Jumbotron from 'react-bootstrap/Jumbotron';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import Row from 'react-bootstrap/Row';
import Spinner from 'react-bootstrap/Spinner';

import './App.css';

// TODO: optional redirect after deploy
const redirect = false;

function App() {
  const deployInput = useRef(null);
  const [showSpinner, setShowSpinner] = useState(false);
  const [focused, setFocused] = useState(false);
  const [status, setStatus] = useState({msg: '', variant: ''});

  const onFocus = () => setFocused(true);
  const onBlur = () => setFocused(false);

  const handleSubmit = async event => {
    setShowSpinner(true);
    event.preventDefault();
    const {target} = event;
    const formData = new FormData(target);
    // IDEA: Show two readonly input boxes on UI, one for site id, another for deploy id
    // decode magstripe on frontend, populate boxes, then submit
    try {
      const body = JSON.stringify(Object.fromEntries(formData));
      if (!body.deployData || body.deployData.length === 0) {
        // Ignore if empty
        return;
      }
      const response = await fetch('/.netlify/functions/deploy', {
        method: 'POST',
        body,
      });
      if (response.ok) {
        const {url} = await response.json();
        if (url) {
          setStatus({msg: `Deployed ${url}`, variant: 'success'});
          if (redirect) {
            window.location.href = url;
          }
        }
      } else {
        setStatus({msg: 'Error submitting deploy request', variant: 'danger'});
      }
    } catch (e) {
      console.log(e);
    } finally {
      setShowSpinner(false);
      if (deployInput.current) {
        deployInput.current.focus();
      }
    }
  };

  useEffect(() => {
    if (deployInput.current) {
      deployInput.current.focus();
    }
  }, [deployInput, focused]);

  useEffect(() => {
    if (focused) {
      setStatus({
        msg: 'System ready.  Swipe your deploy card',
        variant: 'info',
      });
    } else {
      setStatus({
        msg:
          "Not focused; Don't swipe.  Refresh if you return to the browser and this doesn't go away",
        variant: 'warning',
      });
    }
  }, [focused]);

  return (
    <>
      <Navbar collapseOnSelect expand="sm" bg="dark" variant="dark">
        <Navbar.Brand>Swipe Deploy</Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="mr-auto">
            <Nav.Link href="https://gitlab.com/bettse/swipe-deploy/">
              Sourcecode (GitLab)
            </Nav.Link>
          </Nav>
          <Nav>
            <Navbar.Text>
              <img
                src="https://api.netlify.com/api/v1/badges/feab6ed3-a748-445c-8a56-c2ae3e422202/deploy-status"
                alt="Netlify Status"
              />
            </Navbar.Text>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
      <Container fluid="md" className="mt-3">
        <Row>
          <Col>
            <Jumbotron>
              <h1>Swipe to Deploy</h1>
              <p>
                This site allows you to set the deploy of a Netlify site by
                swiping a magstripe card. Encode the magstripe card with the
                site id and deploy id (ex{' '}
                <code>
                  %AAFEAB6ED3-A748-445C-8A56-C2AE3E422202^60C2E8EE22B90200076C055F?
                </code>
                ). Connect a magstripe reader that emulates a keyboard to your
                computer (I use an inexpensive MSR90). This page has a hidden
                text input field with keyboard focus on load. Swipe your card
                and be amazed.
              </p>
            </Jumbotron>
          </Col>
        </Row>
        <Row className="text-center justify-content-center">
          <Col xs={6}>
            <When condition={status.msg !== ''}>
              <Alert variant={status.variant}>{status.msg}</Alert>
            </When>
          </Col>
        </Row>
        <Row>
          <Col className="text-center">
            <If condition={showSpinner}>
              <Then>
                <Spinner
                  animation="grow"
                  variant="primary"
                  style={{height: 128, width: 128}}
                />
              </Then>
              <Else>
                <Form onSubmit={handleSubmit}>
                  <Form.Control
                    id="deployData"
                    ref={deployInput}
                    onFocus={onFocus}
                    onBlur={onBlur}
                    name="deployData"
                    type="text"
                  />
                </Form>
              </Else>
            </If>
          </Col>
        </Row>
        <Row>
          <Col>
            <Accordion>
              <Card className="text-center">
                <Accordion.Toggle as={Card.Header} eventKey="0">
                  Click for demo video
                </Accordion.Toggle>
                <Accordion.Collapse eventKey="0">
                  <Card.Body>
                    <video
                      type="video/mp4"
                      controls
                      src={process.env.PUBLIC_URL + '/demo.mp4'}
                    />
                  </Card.Body>
                </Accordion.Collapse>
              </Card>
            </Accordion>
          </Col>
        </Row>
      </Container>
      <footer className="footer font-small mx-auto">
        <Container fluid className="text-center">
          <Row noGutters>
            <Col>
              <small className="text-muted">
                Hosted with <a href="https://www.netlify.com/">Netlify</a>
              </small>
            </Col>
          </Row>
        </Container>
      </footer>
    </>
  );
}

export default App;
