# Swipe Deploy

Deploy using a magstripe card

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

[![Netlify Status](https://api.netlify.com/api/v1/badges/feab6ed3-a748-445c-8a56-c2ae3e422202/deploy-status)](https://app.netlify.com/sites/tender-tesla-5745f3/deploys)

![video demonstration](public/demo.mp4)

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.
